<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "contohpwdb";

$conn = new mysqli($servername,$username,
    $password,$dbname);

if($conn->connect_error){
    die("Koneksi gagal");
}

//echo "Koneksi berhasil dibuat !";

$sql = "insert into kategori(Nama) values('Indie')";

if($conn->query($sql)===TRUE){
    $last_id = $conn->insert_id;
    echo "Berhasil menambahkan data kategori ".
    "dengan id $last_id";
}else {
    echo "Error: ".$sql."<br>".$conn->error;
}

$conn->close();
?>