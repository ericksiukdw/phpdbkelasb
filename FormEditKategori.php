<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Form Edit Kategori</title>
</head>
<body>
    <h3>Form Edit Kategori</h3>
    <form action="ProsesEditKategori.php" 
        method="POST">
        <label for="katid">Kategori ID</label><br>
        <input type="text" name="katid"><br><br>
        <label for="nama">Nama Kategori</label><br>
        <input type="text" name="nama"><br><br>
        <button type="submit" name="edit">Edit</button>
    </form>
</body>
</html>