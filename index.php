<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contoh PHP</title>
</head>
<body>
    <?php
        function HitungLuasSegitiga($alas,$tinggi){
            return 0.5*$alas*$tinggi;
        }

        $firstname = "Erick";
        $lastname = "Kurniawan";

        $bil1 = 20;
        $bil2 = 25.5;
        $hasil = $bil1+$bil2;

        $luasST = HitungLuasSegitiga(20,34);

        echo "Luas Segitiga : $luasST <br>";
        echo "Nama anda :".$firstname." ".$lastname."<br>";
        echo "Hasil : $bil1 + $bil2 = $hasil <br>";

        echo "Hello World !";
    ?>
</body>
</html>