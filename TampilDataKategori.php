<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "contohpwdb";

$conn = new mysqli($servername,$username,
    $password,$dbname);

if($conn->connect_error){
    die("Koneksi gagal");
}

echo "<h3>Daftar Kategori</h3>";

$sql = "select * from Kategori order by Nama asc";
$result = $conn->query($sql);

if($result->num_rows>0){
echo "<table border='1'>";
echo "<tr><th>Kode</th><th>Nama</th></tr>";
    while($row=$result->fetch_assoc()){
        echo "<tr><td>".$row["KategoriID"]."</td>".
        "<td>".$row["Nama"]."</td></tr>";
    }
echo "</table>";
} else {
    echo "Tidak dapat menampilkan record";
}

$conn->close();
?>