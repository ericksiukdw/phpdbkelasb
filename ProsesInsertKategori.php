<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "contohpwdb";

$conn = new mysqli($servername,$username,
    $password,$dbname);

if($conn->connect_error){
    die("Koneksi gagal");
}

//ambil dari form
$nama = $_POST["nama"];

$stmt = 
$conn->prepare("insert into kategori(Nama) values(?)");
$stmt->bind_param("s",$nama);

try{
    $stmt->execute();
    echo "Data Kategori ".$nama.
        " berhasil ditambah";
}catch(Exception $e){
    echo "Error ".$e->getMessage()."\n";
}finally{
    $conn->close();
}
?>